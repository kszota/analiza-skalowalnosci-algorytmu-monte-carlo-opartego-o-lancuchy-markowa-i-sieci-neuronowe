#!/usr/bin/env python3
#
# Solving Statistical Mechanics using Variational Autoregressive Networks
# 2d classical Ising model

import time

import matplotlib.pyplot as plt

import numpy as np
import torch
import torchvision
from numpy import sqrt
from torch import nn

import itertools

import ising
from args import args
from bernoulli import BernoulliMixture
from made import MADE
from pixelcnn import PixelCNN
from utils import (
    clear_checkpoint,
    clear_log,
    get_last_checkpoint_step,
    ignore_param,
    init_out_dir,
    my_log,
    print_args,
)

from tensor_operations import (
    get_point,
    get_list_of_points,
    set_list_of_points,
    divide_into_4small_squares2,
    combine_into_1big_square2,
    divide_into_4small_squares_universal,
    combine_into_1big_square_universal,
    get_border_and_cross,
    set_cross_values,
    get_border_and_cross_logprob,
    get_border_and_cross_split,
    get_initial_border,
    set_initial_border,
    create_all_possible_configurations
)
import constants
import random

from itertools import chain

from made import MADE_i
from made import MADE_b

def true_dist(L):
    perm = [list(seq) for seq in itertools.product("01", repeat=L*L)]
    perm = [[int(x) for x in value] for i, value in enumerate(perm)]
    perm = torch.tensor(np.array(perm).reshape(2**(L*L), L, L))
    perm = torch.tensor(np.where(perm==1.0, perm, -1.0))
    return perm


def main():
    start_time = time.time()

    init_out_dir()
    if args.clear_checkpoint:
        clear_checkpoint()
    last_step = get_last_checkpoint_step()
    if last_step >= 0:
        my_log('\nCheckpoint found: {}\n'.format(last_step))
    else:
        clear_log()
    print_args()

    if args.net == 'made':
        '''net = MADE(**vars(args))'''
        net_b = MADE_b(args.L,2,args.net_depth,args.net_width,args.bias,args.z2,False,args.res_block,args.x_hat_clip,args.epsilon,args.device) #n_block = 2
        net_b.to(args.device)
        nets_i = []
        params_i = list()
        n_i_nets=int(np.log2(args.L)-1)  #ostatni spin tez ustalamy siecia
        my_log('number of int nets: {}\n'.format(n_i_nets))
        blocks_widths=args.L//2**np.arange(1,n_i_nets+1)-1
        my_log('crosses sizes Li: {}\n'.format(blocks_widths))
        for i in range(n_i_nets):
            net_i = MADE_i(blocks_widths[i],args.net_depth,args.net_width,args.bias,args.z2,False,args.res_block,args.x_hat_clip,args.epsilon,args.device)
            net_i.to(args.device)
            params_ii = list(net_i.parameters())
            params_ii = list(filter(lambda p: p.requires_grad, params_ii))   # take only those parameters from params which has .requires_grad =True
            nparams_ii = int(sum([np.prod(p.shape) for p in params_ii]))
            my_log('Interior net - number of trainable parameters: {}\n'.format(nparams_ii))
            nets_i.append(net_i)
            params_i = params_i + params_ii
    elif args.net == 'pixelcnn':
        net = PixelCNN(**vars(args))
    elif args.net == 'bernoulli':
        net = BernoulliMixture(**vars(args))
    else:
        raise ValueError('Unknown net: {}'.format(args.net))

    params_b = list(net_b.parameters())
    params_b = list(filter(lambda p: p.requires_grad, params_b))   # take only those parameters from params which has .requires_grad =True
    nparams_b = int(sum([np.prod(p.shape) for p in params_b]))
    params = params_b
    my_log('Boundary net - number of trainable parameters: {}\n'.format(nparams_b))

    params = params + params_i

    nparams = int(sum([np.prod(p.shape) for p in params]))

    my_log('Total number of trainable parameters: {}'.format(nparams))
    '''named_params = list(net.named_parameters())'''

    my_log('{}\n'.format(net_b))
    my_log('{}\n'.format(nets_i))

    if args.optimizer == 'sgd':
        optimizer = torch.optim.SGD(params, lr=args.lr)
    elif args.optimizer == 'sgdm':
        optimizer = torch.optim.SGD(params, lr=args.lr, momentum=0.9)
    elif args.optimizer == 'rmsprop':
        optimizer = torch.optim.RMSprop(params, lr=args.lr, alpha=0.99)
    elif args.optimizer == 'adam':
        optimizer = torch.optim.Adam(params, lr=args.lr, betas=(0.9, 0.999))
    elif args.optimizer == 'adam0.5':
        optimizer = torch.optim.Adam(params, lr=args.lr, betas=(0.5, 0.999))
    else:
        raise ValueError('Unknown optimizer: {}'.format(args.optimizer))

    if args.lr_schedule:
        # 0.92**80 ~ 1e-3
        scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
            optimizer, factor=0.92, patience=100, threshold=1e-4, min_lr=1e-6)

    if args.final_step != 0:
        if args.load_optim is False:
            state = torch.load('saved_state_b_L='+str(args.L)+'_beta='+str(args.beta)+'_step='+str(args.final_step)+'_mn.out')    
            # ~ ignore_param(state['net'], net_b)
            net_b.load_state_dict(state['net'])
        
            # ~ optimizer.load_state_dict(state['optimizer'])
        else:
            state = torch.load('saved_state_optimizer_b_L='+str(args.L)+'_beta='+str(args.beta)+'_step='+str(args.final_step)+'_mn.out') 
            # ~ ignore_param(state['net'], net_b)
            net_b.load_state_dict(state['net'])
        
            optimizer.load_state_dict(state['optimizer'])
            my_log('Optimizer state loaded!')

        last_step = state['last_step']

        for k in range(n_i_nets):
            state = torch.load('saved_state_intnet'+str(k)+'_L='+str(args.L)+'_beta='+str(args.beta)+'_step='+str(args.final_step)+'_mn.out')
            # ~ ignore_param(state['net'], nets_i[k])
            nets_i[k].load_state_dict(state['net'])
        
        my_log('State loaded, starting from epoch: {}'.format(last_step))


    init_time = time.time() - start_time
    my_log('init_time = {:.3f}'.format(init_time))

    my_log('Training...\n')
    sample_time = 0
    train_time = 0
    start_time = time.time()
    for step in range(last_step + 1, args.max_step + 1):
        last_step = step

        #Ladowanie najmniejszej sieci nauczonej na L=8 (5000 epok) po 100 epokach dla L=16
        '''if step == 100:
            if args.load_optim is False:
                state = torch.load('saved_state_intnet1_L=8_beta='+str(args.beta)+'_step=5000_mn.out')
                # ~ ignore_param(state['net'], nets_i[k])
                nets_i[2].load_state_dict(state['net'])
                print('Optimizer state NOT loaded!')
            else:
                #state = torch.load('saved_state_intnet1_L=8_beta='+str(args.beta)+'_step=5000_mn.out')
                # ~ ignore_param(state['net'], nets_i[k])
                #nets_i[2].load_state_dict(state['net'])
                state = torch.load('saved_state_optimizer_b_L=8_beta='+str(args.beta)+'_step=5000_mn.out')
                nets_i[2].load_state_dict(state['net'])
                #ignore_param(state['net'], net_b)
                optimizer.load_state_dict(state['optimizer'])
                print('Optimizer state loaded!')'''


        optimizer.zero_grad()
        sample_start_time = time.time()

        with torch.no_grad():
            sample_border = torch.zeros([args.batch_size, 1, args.L, args.L], dtype=torch.float32, device=args.device)
            initial_border = net_b.sample(args.batch_size, args.beta)
            sample_border = set_initial_border(sample_border, initial_border)


            squares = divide_into_4small_squares2(sample_border) #pierwsze dzielenie na 4 kwadraty (specjalny przyapdek pierwszej konfiguracji)

            
            firstvalues = []

            for square in squares:
                lists = get_border_and_cross(square, args.device)
                firstvalues.append(lists)
            

            valuesfromnet = []
            for i in range(len(firstvalues)):
                zmienna = nets_i[0].sample(args.batch_size, args.beta, firstvalues[i])
                valuesfromnet.append(zmienna)

            for i in range(len(squares)):
                squares[i] = set_cross_values(squares[i], valuesfromnet[i])

            
            #PODZIAL DO KONCA
            jj = 1
            while True:
                squares_universal = []
                for square in squares:
                    squares_test = divide_into_4small_squares_universal(square)
                    squares_universal.append(squares_test)
                squares = list(chain.from_iterable(squares_universal))

                values = []
                for square in squares:
                    lists = get_border_and_cross(square, args.device)
                    values.append(lists)

                values_net = []
                for i in range(len(values)):
                    zmienna = nets_i[jj].sample(args.batch_size, args.beta, values[i])
                    values_net.append(zmienna)
                jj = jj + 1


                for i in range(len(squares)):
                    squares[i] = set_cross_values(squares[i], values_net[i])


                if len(squares[0][0][0][0]) == 3:
                    break


            #SKLADANIE DO POCZATKU (oprocz ostatniej konfiguracji)
            while True:
                squares = combine_into_1big_square_universal(squares, args.device)
                if len(squares) == 4:
                    break
            squares = combine_into_1big_square2(squares, args.device)

        assert not squares.requires_grad
        sample_time += time.time() - sample_start_time
        #START LICZENIA PRAWDOPODOBIENSTW

        #SYMETRIE
        log_prob_all = torch.zeros([4,4,2,2,args.batch_size], dtype=torch.float32, device=args.device)
        #count = 0                 ny,nx,q,rr

        for n_y in range(0,4): #1. translacja
            if(n_y == 0):
                sample_inv = squares
            if(n_y == 1):
                sample_inv = torch.roll(squares, shifts=int(args.L/4), dims=2) #dzielenie na 4 symetrie (4)
            for q in range(0,2): #2. transformacja
                if(q == 1):
                    sample_inv_q = -sample_inv
                if(q == 0):
                    sample_inv_q = sample_inv   #(2 mpzliwosci -> 4*2 = 8)
                for n_x in range(0,4): #3. translacja
                    if(n_x == 0):
                        sample_inv_q_x = sample_inv_q
                    if(n_x == 1):
                        sample_inv_q_x = torch.roll(sample_inv_q, shifts=int(args.L/4), dims=3) #dzielenie na 4 -> 8*4 = 32
                    for rr in range(0,2): #4. transformacja
                        if(rr==0):
                            sample_inv_q_x_rr = sample_inv_q_x
                        if(rr==1):
                            sample_inv_q_x_rr = sample_inv_q_x.permute(0,1,3,2) #2 mozliwosci -> 32*2 = 64 (na koncu odejmowane)

                        initial_values = get_initial_border(sample_inv_q_x_rr, args.device)
                        valuesfromnet_lp = net_b.log_prob(initial_values, args.beta)
                        squares_log_prob = divide_into_4small_squares2(sample_inv_q_x_rr)

                        firstvalues_lp_c, firstvalues_lp_b = [], []
                        for square in squares_log_prob:
                            lists_c, lists_b = get_border_and_cross_split(square, args.device)
                            firstvalues_lp_c.append(lists_c)
                            firstvalues_lp_b.append(lists_b)

                        for i in range(len(squares_log_prob)):

                            valuesfromnet_lp += nets_i[0].log_prob(firstvalues_lp_c[i], firstvalues_lp_b[i], args.beta)


                        jjj = 1
                        while True:
                            squares_universal_lp = []
                            for square in squares_log_prob:
                                squares_test = divide_into_4small_squares_universal(square)
                                squares_universal_lp.append(squares_test)
                            squares_log_prob = list(chain.from_iterable(squares_universal_lp))

                            values_lp_c, values_lp_b = [], []
                            for square in squares_log_prob:
                                lists_c, lists_b = get_border_and_cross_split(square, args.device)
                                values_lp_c.append(lists_c)
                                values_lp_b.append(lists_b)


                            for i in range(len(squares_log_prob)):

                                valuesfromnet_lp += nets_i[jjj].log_prob(values_lp_c[i], values_lp_b[i], args.beta)
                            jjj = jjj + 1

                            if len(squares_log_prob[0][0][0][0]) == 3:
                                break

                        log_prob_all[n_y,n_x,q,rr,:] = valuesfromnet_lp

        log_prob = torch.logsumexp(log_prob_all, dim=(0,1,2,3))
        log_prob = log_prob - np.log(64)

        train_start_time = time.time()

        # 0.998**9000 ~ 1e-8
        beta = args.beta * (1 - args.beta_anneal**step)

        with torch.no_grad():
            energy = ising.energy(squares, args.ham, args.lattice,
                                  args.boundary)
            loss = log_prob + beta * energy

        assert not energy.requires_grad
        assert not loss.requires_grad

        loss_reinforce = torch.mean((loss - loss.mean()) * log_prob)
        loss_reinforce.backward()


        if args.clip_grad:
            nn.utils.clip_grad_norm_(params, args.clip_grad)

        optimizer.step()

        if args.lr_schedule:
            scheduler.step(loss.mean())

        train_time += time.time() - train_start_time


        if args.print_step and step % args.print_step == 0:
            free_energy_mean = loss.mean() / args.beta / args.L**2
            free_energy_std = loss.std() / args.beta / args.L**2
            entropy_mean = -log_prob.mean() / args.L**2
            energy_mean = energy.mean() / args.L**2
            mag = squares.mean(dim=0)
            mag_mean = mag.mean()
            mag_sqr_mean = (mag**2).mean()
            if step > 0:
                sample_time /= args.print_step
                train_time /= args.print_step
            used_time = time.time() - start_time
            my_log(
                'step = {}, F = {:.8g}, F_std = {:.8g}, S = {:.8g}, E = {:.8g}, M = {:.8g}, Q = {:.8g}, lr = {:.3g}, beta = {:.8g}, sample_time = {:.3f}, train_time = {:.3f}, used_time = {:.3f}'
                .format(
                    step,
                    free_energy_mean.item(),
                    free_energy_std.item(),
                    entropy_mean.item(),
                    energy_mean.item(),
                    mag_mean.item(),
                    mag_sqr_mean.item(),
                    optimizer.param_groups[0]['lr'],
                    beta,
                    sample_time,
                    train_time,
                    used_time,
                ))
            sample_time = 0
            train_time = 0

            if args.save_sample:
                state = {
                    'sample': squares,
                    'log_prob': log_prob,
                    'energy': energy,
                    'loss': loss,
                }
                torch.save(state, '{}_save/{}.sample'.format(
                    args.out_filename, step))
            

        if (args.out_filename and args.save_step and step % args.save_step == 0):
            state = {
                    'net': net_b.state_dict(),
                    # ~ 'optimizer': optimizer.state_dict(),
                    'last_step': last_step
                    }
            torch.save(state, 'saved_state_b_L='+str(args.L)+'_beta='+str(args.beta)+'_step='+str(step)+'_mn.out')


            state = {
                    'net': net_b.state_dict(),
                    'optimizer': optimizer.state_dict(),
                    }
            torch.save(state, 'saved_state_optimizernetb_b_L='+str(args.L)+'_beta='+str(args.beta)+'_step='+str(step)+'_mn.out')

            state = {
                    'net': nets_i[1].state_dict(), #dla L=8
                    'optimizer': optimizer.state_dict(),
                    }
            torch.save(state, 'saved_state_optimizerneti1_b_L='+str(args.L)+'_beta='+str(args.beta)+'_step='+str(step)+'_mn.out')


            for k in range(n_i_nets):
                state = {
                        'net': nets_i[k].state_dict(),
                        }
                torch.save(state, 'saved_state_intnet'+str(k)+'_L='+str(args.L)+'_beta='+str(args.beta)+'_step='+str(step)+'_mn.out')


        if (args.out_filename and args.visual_step
                and step % args.visual_step == 0):
            torchvision.utils.save_image(
                sample,
                '{}_img/{}.png'.format(args.out_filename, step),
                nrow=int(sqrt(sample.shape[0])),
                padding=0,
                normalize=True)

            if args.print_sample:
                x_hat_np = x_hat.view(x_hat.shape[0], -1).cpu().numpy()
                x_hat_std = np.std(x_hat_np, axis=0).reshape([args.L] * 2)

                x_hat_cov = np.cov(x_hat_np.T)
                x_hat_cov_diag = np.diag(x_hat_cov)
                x_hat_corr = x_hat_cov / (
                    sqrt(x_hat_cov_diag[:, None] * x_hat_cov_diag[None, :]) +
                    args.epsilon)
                x_hat_corr = np.tril(x_hat_corr, -1)
                x_hat_corr = np.max(np.abs(x_hat_corr), axis=1)
                x_hat_corr = x_hat_corr.reshape([args.L] * 2)

                energy_np = energy.cpu().numpy()
                energy_count = np.stack(
                    np.unique(energy_np, return_counts=True)).T



            if args.print_grad:
                my_log('grad max_abs min_abs mean std')
                for name, param in named_params:
                    if param.grad is not None:
                        grad = param.grad
                        grad_abs = torch.abs(grad)
                        my_log('{} {:.3g} {:.3g} {:.3g} {:.3g}'.format(
                            name,
                            torch.max(grad_abs).item(),
                            torch.min(grad_abs).item(),
                            torch.mean(grad).item(),
                            torch.std(grad).item(),
                        ))
                    else:
                        my_log('{} None'.format(name))
                my_log('')


if __name__ == '__main__':
    main()
