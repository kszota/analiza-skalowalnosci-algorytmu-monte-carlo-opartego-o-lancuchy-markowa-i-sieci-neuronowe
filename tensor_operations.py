import torch
import numpy as np
import random
from math import isqrt
import constants

import itertools
from itertools import chain
from itertools import combinations_with_replacement
from itertools import permutations
from itertools import product

def create_tensor(L):
    tensor = torch.zeros([L,L], dtype=torch.float64)
    return tensor
    #pass

def get_tensor_element(tensor, y, x):
    #element = tensor[y][x].item()
    element = tensor[y][x].numpy()
    return element
    #pass

def get_tensor_column(tensor, y, L):
    column = np.zeros([L])
    for i in range(L):
        column[i] = tensor[i][y].numpy()
    return column
    #pass

def get_tensor_row(tensor, x, L):
    row = tensor[x].numpy()
    return row
    #pass

def set_tensor_element(tensor, element, y, x):
    tensor[y][x] = element
    #pass

def get_point(sample, coordinates, batch=0):
    try:
        point = sample[:, :, coordinates[0], coordinates[1]]
        return point
    except IndexError:
        print('Niewlasciwe wspolrzedne dla danej konfiguracji o rozmiarze {}x{}: {}'.format(isqrt(len(sample[:, :])),isqrt(len(sample[:, :])),coordinates))
    #pass

def get_list_of_points(sample, coordinates, batch=0):
    list_of_points = np.zeros((len(sample),len(coordinates)))
    for j in range(len(sample)):
        for i in range(len(coordinates)):
            list_of_points[j][i] = sample[j, :, coordinates[i][0], coordinates[i][1]]
    return list_of_points
    
    #pass

def set_list_of_points(sample, coordinates, batch=0):
    try:
        list_of_points = torch.zeros([len(sample), 1, isqrt(len(coordinates)), isqrt(len(coordinates))], dtype=torch.float32)
        for j in range(len(sample)):
            for i in range(len(coordinates)):
                list_of_points[j, :, coordinates[i][0], coordinates[i][1]] = sample[j, :, constants.LIST_OF_POSITIONS_FIXED[i][0], constants.LIST_OF_POSITIONS_FIXED[i][1]]
        return list_of_points
    except IndexError:
        print('Niewlasciwe wspolrzedne dla danej konfiguracji o rozmiarze {}x{}: {}'.format(isqrt(len(sample[:, :])),isqrt(len(sample[:, :])),coordinates[i]))
    #pass

def divide_into_4small_squares(sample):
    L = sample.shape[2] # lub sample.shape[3]
    S11 = sample[:,:,:int(L/2),:int(L/2)]
    S12 = sample[:,:,:int(L/2),int(L/2):]
    S21 = sample[:,:,int(L/2):,:int(L/2)]
    S22 = sample[:,:,int(L/2):,int(L/2):]
    return [S11, S12, S21, S22]
    #pass

def combine_into_1big_square(squares, device):
    L = len(squares[0][0][0][0]) + len(squares[1][0][0][0]) #rozmiar pierwszego rzedu kwadratu S11 + rozmiar pierwszego rzedu kwadratu S12
    sample = torch.zeros([len(squares[0]), 1, L, L], dtype=torch.float32, device=device) #len(squares[0]) -> batch_size
    for batch in range(len(sample)):
        x,y = torch.cat((squares[0][batch][0],squares[1][batch][0],squares[2][batch][0],squares[3][batch][0]),dim=1).t().chunk(2)
        sample[batch] = torch.cat((x,y),dim=1).t()
    return sample
    #pass

def divide_into_4small_squares2(sample):
    L = sample.shape[2] # lub sample.shape[3]
    sample = torch.roll(sample,shifts=(1, 0),dims=(2,3))
    S11 = sample[:,:,:int(L/2)+1,:int(L/2)+1]
    sample = torch.roll(sample,shifts=(0, -1),dims=(2,3))
    S12 = sample[:,:,:int(L/2)+1,int(L/2)-1:]
    sample = torch.roll(sample,shifts=(-1, 1),dims=(2,3))
    S21 = sample[:,:,int(L/2)-1:,:int(L/2)+1]
    sample = torch.roll(sample,shifts=(0, -1),dims=(2,3))
    S22 = sample[:,:,int(L/2)-1:,int(L/2)-1:]
    return [S11, S12, S21, S22]
    #pass

def combine_into_1big_square2(squares, device):
    #w kazdym samplu trzeba uciac górę i prawo
    L = len(squares[0][0][0][0]) + len(squares[1][0][0][0]) #rozmiar pierwszego rzedu kwadratu S11 + rozmiar pierwszego rzedu kwadratu S12
    L = L - 2 #bo ucinamy gore i prawo
    sample = torch.zeros([len(squares[0]), 1, L, L], dtype=torch.float32, device=device) #len(squares[0]) -> batch_size

    for square in range(len(squares)):
        squares[square] = squares[square][:,:,1:,:-1]

    for batch in range(len(sample)):
        x,y = torch.cat((squares[0][batch][0],squares[1][batch][0],squares[2][batch][0],squares[3][batch][0]),dim=1).t().chunk(2)
        sample[batch] = torch.cat((x,y),dim=1).t()
    return sample
    #pass

def divide_into_4small_squares_universal(sample):
    L = sample.shape[2] # lub sample.shape[3]
    S11 = sample[:,:,:int(L/2)+1,:int(L/2)+1]
    S12 = sample[:,:,:int(L/2)+1,int(L/2):]
    S21 = sample[:,:,int(L/2):,:int(L/2)+1]
    S22 = sample[:,:,int(L/2):,int(L/2):]
    return [S11, S12, S21, S22]
    #pass

def list_of_lists(lst, size):
    return [lst[i:i+size] for i in range(0, len(lst), size)]

def combine_into_1big_square_universal(all_squares, device):
    #w samplach trzeba uciąć wewnętrzny krzyż żeby nie było duplikatoów
    lists_of_squares = list_of_lists(all_squares, 4)    #tworze z jednej duzej listy tensorow, listy po 4 tensory kazda z zachowana kolejnoscia
    samples = []    #majac np. 64 tensory (16 list po 4 tensory) -> dostaje 16 tensorow (4 listy po 4 tensory)

    for squares in lists_of_squares:
        L = len(squares[0][0][0][0]) + len(squares[1][0][0][0]) #rozmiar pierwszego rzedu kwadratu S11 + rozmiar pierwszego rzedu kwadratu S12
        sample = torch.zeros([len(squares[0]), 1, L, L], dtype=torch.float32, device=device) #len(squares[0]) -> batch_size

        for batch in range(len(sample)):
            x,y = torch.cat((squares[0][batch][0],squares[1][batch][0],squares[2][batch][0],squares[3][batch][0]),dim=1).t().chunk(2)
            sample[batch] = torch.cat((x,y),dim=1).t()

        sample = torch.cat((sample[:,:,:,:int(L/2)], sample[:,:,:,int(L/2)+1:]), dim=3)
        sample = torch.cat((sample[:,:,:int(L/2),:], sample[:,:,int(L/2)+1:,:]), dim=2)
        #usuniecie "krzyza"

        samples.append(sample)

    return samples
    #pass

def get_border_and_cross(sample, device):
    values_c = []   #krzyz w srodku
    L = sample.shape[2] # lub sample.shape[3]
    values_b = torch.zeros([sample.shape[0], 1, 4*(L-1)-4], dtype=torch.float32, device=device)
    ii = 0
    for i in range(L):
        for j in range(L):
            #pobieranie wartosci krawedzie
            if (j == 0 and i != 0 and i != L-1) or (i == 0 and j != 0 and j != L-1) or (j == L-1 and i != 0 and i != L-1) or (i == L-1 and j != 0 and j != L-1):
                values_b[:,:,ii] = sample[:,:,i,j]
                ii = ii + 1
    return values_b
    #pass

def get_border_and_cross_logprob(sample, device):
    L = sample.shape[2] # lub sample.shape[3]
    values_a = torch.zeros([sample.shape[0], 1, (4*(L-1)-4) + (2*(L-1)-3)], dtype=torch.float32, device=device)
    ii = 0

    for i in range(L):
        for j in range(L):
            #pobieranie wartosci krawedzie
            if (j == 0 and i != 0 and i != L-1) or (i == 0 and j != 0 and j != L-1) or (j == L-1 and i != 0 and i != L-1) or (i == L-1 and j != 0 and j != L-1):
                values_a[:,:,ii] = sample[:,:,i,j]
                ii = ii + 1

    for i in range(L):
        for j in range(L):
            #pobieranie wartosci krzyzyka w srodku
            if (j == L//2 and i != 0 and i != L-1) or (i == L//2 and j != 0 and j != L-1):
                values_a[:,:,ii] = sample[:,:,i,j]
                ii = ii + 1

    return values_a
    #pass

def get_border_and_cross_split(sample, device):
    L = sample.shape[2] # lub sample.shape[3]
    values_b = torch.zeros([sample.shape[0], 1, 4*(L-1)-4], dtype=torch.float32, device=device)
    values_c = torch.zeros([sample.shape[0], 1, 2*(L-1)-3], dtype=torch.float32, device=device)
    ii = 0
    jj = 0

    for i in range(L):
        for j in range(L):
            #pobieranie wartosci krawedzie
            if (j == 0 and i != 0 and i != L-1) or (i == 0 and j != 0 and j != L-1) or (j == L-1 and i != 0 and i != L-1) or (i == L-1 and j != 0 and j != L-1):
                values_b[:,:,ii] = sample[:,:,i,j]
                ii = ii + 1

    for i in range(L):
        for j in range(L):
            #pobieranie wartosci krzyzyka w srodku
            if (j == L//2 and i != 0 and i != L-1) or (i == L//2 and j != 0 and j != L-1):
                values_c[:,:,jj] = sample[:,:,i,j]
                jj = jj + 1

    return values_c, values_b
    #pass

def set_cross_values(sample, values):
    L = sample.shape[2] # lub sample.shape[3]
    val = 0

    for i in range(L):
        for j in range(L):
            if (j == L//2 and i != 0 and i != L-1) or (i == L//2 and j != 0 and j != L-1):
                sample[:, :, i, j] = values[:,:,val]
                val = val + 1

    return sample
    #pass

def get_initial_border(sample, device):
    L = sample.shape[2] 
    spins = 4 * L - 4
    red_cross = torch.zeros([sample.shape[0], 1, spins], dtype=torch.float32, device=device)

    ii = 0
    for i in range(L):
        for j in range(L):
            if j == 0 or j == L//2 or i == L//2 -1 or i == L - 1:    # bo L + L + L + L - 4 = 4L - 4
                red_cross[:,:,ii] = sample[:,:,i,j]
                ii = ii + 1

    return red_cross
    #pass

def set_initial_border(sample, red_cross):
    L = sample.shape[2]
    spins = 4 * L - 4
    
    jj = 0
    for i in range(L):
        for j in range(L):
            if j == 0 or j == L//2 or i == L//2 -1 or i == L - 1:    # bo L + L + L + L - 4 = 4L - 4
                sample[:,:,i,j] = red_cross[:,:,jj]
                jj = jj + 1

    return sample
    #pass

def create_all_possible_configurations(device):
    sample = torch.zeros([32, 1, 3, 3], dtype=torch.float32, device=device) #batch_size -> 2^5=32, L -> 3x3
    
    all_configs = list(product([-1.0, 1.0], repeat=5))

    for i in range(sample.shape[0]):
        sample[i,:,1,1] = all_configs[i][4] 
        sample[i,:,2,1] = all_configs[i][3] 
        sample[i,:,1,2] = all_configs[i][2] 
        sample[i,:,1,0] = all_configs[i][1] 
        sample[i,:,0,1] = all_configs[i][0] 

    return sample
    #pass

def create_all_possible_configurations_bigger(device):
    sample = torch.ones([32, 1, 5, 5], dtype=torch.float32, device=device) #tylko dla krawedzie rownym 1, batch_size==cross -> 2^5=32, L -> 5x5
    
    all_configs = list(product([-1.0, 1.0], repeat=5))

    for i in range(sample.shape[0]):
        #w tym wypadku zmieniam tylko spiny dla krzyzyka
        sample[i,:,2,2] = all_configs[i][4] 
        sample[i,:,3,2] = all_configs[i][3] 
        sample[i,:,2,3] = all_configs[i][2] 
        sample[i,:,2,1] = all_configs[i][1] 
        sample[i,:,1,2] = all_configs[i][0] 

    return sample
    #pass

### CZESC TESTOWA / WYPISYWANIE WYNIKOW / SPRAWDZANIE ###

#for s in range(-4,6,2):
#    print(s, np.log(np.exp(0.6*s) / (np.exp(0.6*s) + np.exp(-0.6*s))))